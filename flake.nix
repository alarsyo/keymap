{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs { inherit system; };
    in {
      devShells.default = (pkgs.buildFHSUserEnv {
        name = "arduino";

        targetPkgs = pkgs: [
          pkgs.ncurses
          pkgs.arduino
          pkgs.arduino-cli
          pkgs.zlib
          (pkgs.python3.withPackages(ps: [
            ps.pyserial
          ]))
        ];

        multiPkgs = null;
      }).env;
    });
}
